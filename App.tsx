/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {createContext, useCallback, useEffect, useState} from 'react';
// import {
//   Alert,
//   SafeAreaView,
//   ScrollView,
//   StatusBar,
//   StyleSheet,
//   Text,
//   View,
// } from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LoginView from './src/Views/LoginView';
import HomeVIew from './src/Views/HomeVIew';
import {RootStackParamList} from './src/Views/ParamList';

import {loginUsername} from './src/Views/LoginView';
// import Button from './src/component/MyButton';
// import MyTextInput from './src/component/MyTextInput';

const Stack = createStackNavigator<RootStackParamList>();
export const UsernameContext = createContext('Default');

const App = () => {
  const x = 'EMPTY';
  return (
    <UsernameContext.Provider value={x}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Login" component={LoginView} />
          <Stack.Screen name="Home" component={HomeVIew} />
        </Stack.Navigator>
      </NavigationContainer>
    </UsernameContext.Provider>
  );
};

export default App;
