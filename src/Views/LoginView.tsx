import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import React, {useCallback, useEffect, useState} from 'react';
import {RootStackParamList} from '../Views/ParamList';

import {
  View,
  Text,
  StyleSheet,
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
} from 'react-native';

import Button from '../component/MyButton';
import MyTextInput from '../component/MyTextInput';

export var loginUsername = '';

type loginViewProp = StackNavigationProp<RootStackParamList, 'Login'>;

const LoginView = () => {
  const navigation = useNavigation<loginViewProp>();

  const xUsername = 'yolosystem';
  const xPassword = 'yolosystem';

  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const [usernameFocus, setUsernameFocus] = useState(true);
  const [passwordFocus, setPasswordFocus] = useState(true);

  useEffect(() => {
    console.log('Demo useEffect');
    Alert.alert('Chao mung den voi Yolo System');
  }, []);

  const loginOnClick = useCallback(() => {
    console.log('Login onClick');
    if (userName === xUsername && password === xPassword) {
      Alert.alert('Dang nhap thanh cong');
      loginUsername = userName;
      console.log(loginUsername);
      navigation.navigate('Home');
    } else {
      Alert.alert('Thong tin sai! Vui long nhap lai');
    }
  }, [userName, password, navigation]);

  const loginFBOnClick = useCallback(() => {
    console.log('Login FB onClick');
  }, []);

  const loginGGOnClick = useCallback(() => {
    console.log('Login GG onClick');
  }, []);

  useEffect(() => {
    console.log('Username is: ', userName);
  }, [userName]);

  useEffect(() => {
    console.log('password is: ', password);
  }, [password]);

  function checkUsernameFocus(x: String) {
    if (x === '') {
      setUsernameFocus(false);
    } else {
      setUsernameFocus(true);
    }
  }

  function checkPasswordFocus(x: String) {
    if (x === '') {
      setPasswordFocus(false);
    } else {
      setPasswordFocus(true);
    }
  }
  return (
    <SafeAreaView>
      <StatusBar barStyle="dark-content" />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View>
          <Text style={styles.highlight}>Yolo System</Text>
          <MyTextInput
            placeholder="Tên đăng nhập"
            defaultValue={userName}
            onChangeText={setUserName}
            onBlur={() => checkUsernameFocus(userName)}
            onFocus={() => setUsernameFocus(true)}
          />
          <Text style={styles.textWaring}>
            {usernameFocus ? null : 'Vui long nhap ten dang nhap'}
          </Text>
          <MyTextInput
            placeholder="Mật khẩu"
            defaultValue={password}
            onChangeText={setPassword}
            onBlur={() => checkPasswordFocus(password)}
            onFocus={() => setPasswordFocus(true)}
          />
          <Text style={styles.textWaring}>
            {passwordFocus ? null : 'Vui long nhap mat khau dang nhap'}
          </Text>
          <Button
            onPress={loginOnClick}
            buttonText="Hello"
            buttonStyle={[styles.myButton, styles.loginButton]}
          />
          <Text style={styles.highlight}>Or</Text>
          <Button
            onPress={loginFBOnClick}
            buttonText="Facebook"
            buttonStyle={[styles.myButton, styles.loginFBButton]}
          />
          <Button
            onPress={loginGGOnClick}
            buttonText="Google"
            buttonStyle={[styles.myButton, styles.loginGGButton]}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
    width: '100%',
    textAlign: 'center',
    fontSize: 48,
    color: 'green',
  },
  passInput: {
    marginVertical: 5,
  },
  myButton: {
    backgroundColor: 'green',
  },
  loginButton: {
    backgroundColor: 'purple',
  },
  loginFBButton: {
    backgroundColor: 'blue',
  },
  loginGGButton: {
    backgroundColor: 'red',
  },
  demoContainer: {
    height: 300,
    backgroundColor: 'yellow',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    opacity: 0.3,
  },
  child: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'black',
    width: 100,
    height: 100,
    backgroundColor: 'red',
  },
  textWaring: {
    fontSize: 10,
    color: 'red',
    marginHorizontal: 15,
  },
});

export default LoginView;
