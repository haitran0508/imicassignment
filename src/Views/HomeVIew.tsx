import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import React, {useContext} from 'react';

import {View, Text, StyleSheet, Button} from 'react-native';
import {RootStackParamList} from './ParamList';
import {UsernameContext} from '../../App';
type homeViewProp = StackNavigationProp<RootStackParamList, 'Home'>;

const HomeVIew = () => {
  const userName = useContext(UsernameContext);
  const navigation = useNavigation<homeViewProp>();
  return (
    <View>
      <Text style={styles.title}>Chao mung {userName}</Text>
      <Button title="Login" onPress={() => navigation.navigate('Login')} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#312e38',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 30,
    color: 'green',
    textAlign: 'center',
    marginVertical: 100,
  },
  highlight: {
    fontWeight: '700',
    width: '100%',
    textAlign: 'center',
    fontSize: 48,
    color: 'green',
  },
});

export default HomeVIew;
